/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.ModuleModel;
import model.ProyectModel;
import model.QueryModel;

/**
 *
 * @author capacitacion
 */
public class CrudQuery {

    Connection conection;

    public CrudQuery() {
        conection = SqliteConnection.Connector();
        if (conection == null) {
            System.exit(1);
        }
    }
    public String getQuery(int idProyect, int idModule, int nroConsulta) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String query = "Select consulta from Consulta where idProyecto = ? and idModulo = ? and nroConsulta = ?";

        try {
            preparedStatement = conection.prepareStatement(query);
            preparedStatement.setString(1, String.valueOf(idProyect));
            preparedStatement.setString(2, String.valueOf(idModule));
            preparedStatement.setString(3, String.valueOf(nroConsulta));

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1);
            }
        } catch (SQLException e) {
        } finally {
            preparedStatement.close();
            resultSet.close();
        }
        return null;
    }
    

    public List<ProyectModel> getComboProyect() throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = conection.prepareStatement(getQuery(2,2,1));
            preparedStatement.setString(1, "2");
            resultSet = preparedStatement.executeQuery();
            return getListProyectModel(resultSet);
        } catch (Exception e) {
            System.out.println(e);
            return null;

        } finally {
            preparedStatement.close();
            resultSet.close();

        }
    }

    public List<ModuleModel> getComboModule(int idProyecto) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = conection.prepareStatement(getQuery(2,2,2));
            preparedStatement.setString(1, String.valueOf(idProyecto));
            resultSet = preparedStatement.executeQuery();
            return getListModuleModel(resultSet);
        } catch (Exception e) {
            System.out.println(e);
            return null;

        } finally {
            preparedStatement.close();
            resultSet.close();

        }
    }

    public List<QueryModel> getComboQuery(int idProyect, int idModule) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = conection.prepareStatement(getQuery(2,2,5));
            preparedStatement.setString(1, String.valueOf(idProyect));
            preparedStatement.setString(2, String.valueOf(idModule));
            resultSet = preparedStatement.executeQuery();
            return getListQueryModel(resultSet);
        } catch (Exception e) {
            System.out.println(e);
            return null;

        } finally {
            preparedStatement.close();
            resultSet.close();
        }

    }

    private List<ProyectModel> getListProyectModel(ResultSet resultSet) throws SQLException {
        List<ProyectModel> listProyectModel = new ArrayList<ProyectModel>();
        ProyectModel proyectModel;
        while (resultSet.next()) {
            proyectModel = new ProyectModel(Integer.parseInt(resultSet.getString("idProyecto")), resultSet.getString("nombreProyecto"));
            listProyectModel.add(proyectModel);
        }
        return listProyectModel;
    }

    private List<ModuleModel> getListModuleModel(ResultSet resultSet) throws SQLException {
        List<ModuleModel> listModuleModel = new ArrayList<ModuleModel>();
        ModuleModel moduleModel;
        while (resultSet.next()) {
            moduleModel = new ModuleModel(Integer.parseInt(resultSet.getString("idModulo")), Integer.parseInt(resultSet.getString("idProyecto")), resultSet.getString("nombreModulo"));
            listModuleModel.add(moduleModel);
        }
        return listModuleModel;
    }

    private List<QueryModel> getListQueryModel(ResultSet resultSet) throws SQLException {
        List<QueryModel> listQueryModel = new ArrayList<QueryModel>();
        QueryModel queryModel;
        while (resultSet.next()) {
            //int idQuery, int idProyect, int idModule, String query, int numberQuery, String description
            queryModel = new QueryModel(Integer.parseInt(resultSet.getString("idConsulta")), Integer.parseInt(resultSet.getString("idProyecto")), Integer.parseInt(resultSet.getString("idModulo")), resultSet.getString("consulta"), Integer.parseInt(resultSet.getString("nroConsulta")), resultSet.getString("descripcion"));
            listQueryModel.add(queryModel);
        }
        return listQueryModel;

    }

    public boolean saveNewQuery(int idProyect, int idModule, String query, String description) throws SQLException {
        PreparedStatement preparedStatement = null;

        try {
            int numberQuery = getMaxNumberQuery(idProyect, idModule) + 1;
            preparedStatement = conection.prepareStatement(getQuery(2,2,3));
            preparedStatement.setString(1, String.valueOf(idProyect));
            preparedStatement.setString(2, String.valueOf(idModule));
            preparedStatement.setString(3, query);
            preparedStatement.setString(4, String.valueOf(numberQuery));
            preparedStatement.setString(5, description);

            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;

        } finally {
            preparedStatement.close();
        }
    }

    private int getMaxNumberQuery(int idProyect, int idModule) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            preparedStatement = conection.prepareStatement(getQuery(2,2,4));
            preparedStatement.setString(1, String.valueOf(idProyect));
            preparedStatement.setString(2, String.valueOf(idModule));
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return Integer.parseInt(resultSet.getString(1));
            }
            return 0;
        } catch (Exception e) {
            System.out.println(e);
            return 0;

        } finally {
            preparedStatement.close();
            resultSet.close();

        }
    }

    public boolean deleteQuery(int idProyect, int idModule, int idNumberQuery) throws SQLException {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conection.prepareStatement(getQuery(2,2,6));
            preparedStatement.setString(1, String.valueOf(idProyect));
            preparedStatement.setString(2, String.valueOf(idModule));
            preparedStatement.setString(3, String.valueOf(idNumberQuery));

            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;

        } finally {
            preparedStatement.close();
        }
    }

    public boolean updateQuery(int idQuery, String query, String description) throws SQLException {
        PreparedStatement preparedStatement = null;

        try {
            preparedStatement = conection.prepareStatement(getQuery(2,2,7));
            preparedStatement.setString(1, query);
            preparedStatement.setString(2, description);
            preparedStatement.setString(3, String.valueOf(idQuery));

            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            System.out.println(e);
            return false;

        } finally {
            preparedStatement.close();
        }
    }
}