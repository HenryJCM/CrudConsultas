/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package repository;

import java.sql.*;

/**
 *
 * @author capacitacion
 */
public class SqliteConnection {

    public static Connection Connector() {

        try {
            Class.forName("org.sqlite.JDBC");
            Connection con = DriverManager.getConnection("jdbc:sqlite:hello.sqlite");
            return con;
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }

    }
}
