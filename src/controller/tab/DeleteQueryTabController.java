/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.tab;

import controller.CrudQueryController;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import model.ModuleModel;
import model.ProyectModel;
import model.QueryModel;
import service.CrudQueryService;

/**
 * El controlador se encarga de manejar la vista "DeleteQueryTab"
 * Es incicializada por la el controlador padre "CrudQueryController"
 * FXML Controller class
 * @author capacitacion
 * @version 1.0 25/08/2016
 * @see crudQueryController.java
 * @see DeleteQueryController.fxml
 * @see ProyectModeljava
 * @see ModuleModel.java
 * @see QueryModel.java
 */
public class DeleteQueryTabController {

    private CrudQueryController crudQueryController;
    @FXML
    private ComboBox idComboProyect;
    @FXML
    private ComboBox idComboModule;
    @FXML
    private ComboBox idComboNumberQuery;
    @FXML
    private TextArea idTextAreaQuery;
    @FXML
    private Button idButtonDelete;
    
    Alert alert = new Alert(Alert.AlertType.INFORMATION);

    
    @FXML
    private void handleButtonActionDeleteQuery(ActionEvent event) throws SQLException {
        QueryModel queryModel = (QueryModel) idComboNumberQuery.getSelectionModel().getSelectedItem();

        if (getService().deleteQuery(queryModel.getIdProyect(), queryModel.getIdModule(), queryModel.getNumberQuery())) {
            refreshDeleteQueryTab();
            refreshOtherTabs();
            alert.setTitle("Confirmacion Eliminar");
            alert.setHeaderText(null);
            alert.setContentText("La consulta fue eliminada de la Base de Datos");
            alert.showAndWait();
        } else {
            alert.setTitle("Error");
            alert.setHeaderText(null);
            alert.setContentText("Hubo un error en la transacción");
            alert.showAndWait();
        }
    }

    public void injectMainController(CrudQueryController mainController) {
        this.crudQueryController = mainController;
    }

    private CrudQueryService getService() {
        return crudQueryController.getService();
    }

    public void init() {
        refreshDeleteQueryTab();
        idComboProyect.getSelectionModel().selectedItemProperty().addListener(getChangeListenerProyectModel());
        idComboModule.getSelectionModel().selectedItemProperty().addListener(getChangeListenerModuleModel());
        idComboNumberQuery.getSelectionModel().selectedItemProperty().addListener(getChangeListenerQueryModel());
    }

    public void refreshDeleteQueryTab() {
        idButtonDelete.setDisable(true);
        idTextAreaQuery.clear();
        clearComboModuleAndNumerQuery();
        loadDataComboProyect();
    }
    
    private void refreshOtherTabs(){
        crudQueryController.refreshAddQueryTab();
        crudQueryController.refreshUpdateQueryTab();
    }

    private void clearComboModuleAndNumerQuery() {
        idComboModule.setValue(null);
        idComboNumberQuery.setValue(null);
        idComboModule.setDisable(true);
        idComboNumberQuery.setDisable(true);
    }

    private ChangeListener<ProyectModel> getChangeListenerProyectModel() {
        ChangeListener<ProyectModel> listener = new ChangeListener<ProyectModel>() {
            @Override
            public void changed(ObservableValue<? extends ProyectModel> ov, ProyectModel oldProyect, ProyectModel newProyect) {

                if (newProyect != null) {
                    loadDataComboModule(newProyect.getIdProyect());
                    idButtonDelete.setDisable(true);
                    idComboNumberQuery.setDisable(true);
                    idComboNumberQuery.setValue(null);
                    idTextAreaQuery.clear();
                }

            }
        };
        return listener;
    }

    private ChangeListener<ModuleModel> getChangeListenerModuleModel() {
        ChangeListener<ModuleModel> listener = new ChangeListener<ModuleModel>() {
            @Override
            public void changed(ObservableValue<? extends ModuleModel> ov, ModuleModel oldModule, ModuleModel newModule) {

                if (newModule != null) {
                    loadDataComboAquery(newModule.getIdProyect(), newModule.getIdModule());
                    idButtonDelete.setDisable(true);
                    idTextAreaQuery.clear();
                }
            }
        };
        return listener;
    }

    private ChangeListener<QueryModel> getChangeListenerQueryModel() {
        ChangeListener<QueryModel> listener = new ChangeListener<QueryModel>() {
            @Override
            public void changed(ObservableValue<? extends QueryModel> ov, QueryModel OldQuery, QueryModel newQuery) {

                if (newQuery != null) {
                    idButtonDelete.setDisable(false);
                    idTextAreaQuery.setText(newQuery.getQuery());
                }
            }
        };
        return listener;
    }

    private void loadDataComboProyect() {
        try {
            List<ProyectModel> listProyectModel = getService().getComboProyect();

            if (listProyectModel != null) {
                ObservableList<ProyectModel> obsList = FXCollections.observableArrayList();
                for (ProyectModel proyect : listProyectModel) {
                    obsList.add(proyect);
                }
                idComboProyect.setItems(obsList);
                idButtonDelete.setDisable(true);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrudQueryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadDataComboModule(int idProyect) {
        try {
            List<ModuleModel> listModuleModel = getService().getComboModule(idProyect);
            if (listModuleModel != null) {
                ObservableList<ModuleModel> obsList = FXCollections.observableArrayList();
                for (ModuleModel module : listModuleModel) {
                    obsList.add(module);
                }
                idComboModule.setDisable(false);
                idComboModule.setItems(obsList);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrudQueryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadDataComboAquery(int idProyect, int idModule) {

        try {
            List<QueryModel> listQueryModel = getService().getComboQuery(idProyect, idModule);

            if (listQueryModel != null) {
                ObservableList<QueryModel> obsList = FXCollections.observableArrayList();
                for (QueryModel query : listQueryModel) {
                    obsList.add(query);
                }
                idComboNumberQuery.setDisable(false);
                idComboNumberQuery.setItems(obsList);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrudQueryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
