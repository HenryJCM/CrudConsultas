/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.tab;

import controller.CrudQueryController;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import model.ModuleModel;
import model.ProyectModel;
import service.CrudQueryService;

/**
 * El controlador se encarga de manejar la vista "AddQueryTab"
 * Es incicializada por el controlador "CrudQueryController"
 * FXML Controller class
 * @author capacitacion
 * @version 1.0 25/08/2016
 * @see crudQueryController.java
 * @see AddQueryController.fxml
 * @see ProyectModeljava
 * @see ModuleModel.java
 */
public class AddQueryTabController {
    
    
    private CrudQueryController crudQueryController;
    @FXML
    private ComboBox idComboProyect;
    @FXML
    private ComboBox idComboModule;
    @FXML
    private TextArea idTextAreaQuery;
    @FXML
    private Button idButtonAdd;
    @FXML
    private TextField idTextDescription;
    
    Alert alert = new Alert(AlertType.INFORMATION);
    
    public void injectMainController(CrudQueryController mainController) {
        this.crudQueryController = mainController;
    }

    private CrudQueryService getService() {
        return crudQueryController.getService();
    }

    @FXML
    private void handleButtonActionAddQuery(ActionEvent event) throws SQLException {
       
        if (!idTextAreaQuery.getText().trim().isEmpty() && !idTextDescription.getText().trim().isEmpty()) {
            ModuleModel moduleModel = (ModuleModel) idComboModule.getSelectionModel().getSelectedItem();

            if (getService().saveNewQuery(moduleModel.getIdProyect(), moduleModel.getIdModule(), idTextAreaQuery.getText(), idTextDescription.getText())) {
                refreshAddQueryTab();
                refreshOtherTabs();
                alert.setTitle("Confirmacion Agregar");
                alert.setHeaderText(null);
                alert.setContentText("La consulta fue agregada a la Base de Datos");
                alert.showAndWait();
            } else {
                alert.setTitle("Error");
                alert.setHeaderText(null);
                alert.setContentText("Hubo un error en la transacción");
                alert.showAndWait();
            }
        } else {
            alert.setTitle("Falta de informacion");
            alert.setHeaderText(null);
            alert.setContentText("Rellene los campos de descripcion y consulta");
            alert.showAndWait();
        }
        idTextAreaQuery.setPromptText("Escriba alguna consulta");
    }
    
    public void init() {
        refreshAddQueryTab();
        idComboProyect.getSelectionModel().selectedItemProperty().addListener(getChangeListenerProyectModel());
        idComboModule.getSelectionModel().selectedItemProperty().addListener(getChangeListenerModuleModel());
    }

    public void refreshAddQueryTab() {
        idButtonAdd.setDisable(true);
        idTextDescription.clear();
        idTextAreaQuery.clear();
        clearComboModule();
        loadDataComboProyect();
    }
    
    private void refreshOtherTabs(){
        crudQueryController.refreshDeleteQueryTab();
        crudQueryController.refreshUpdateQueryTab();
    }

    private void clearComboModule() {
        idComboModule.setValue(null);
        idComboModule.setDisable(true);
    }

    private ChangeListener<ProyectModel> getChangeListenerProyectModel() {
        ChangeListener<ProyectModel> listener = new ChangeListener<ProyectModel>() {
            @Override
            public void changed(ObservableValue<? extends ProyectModel> ov, ProyectModel oldProyect, ProyectModel newProyect) {

                if (newProyect != null) {
                    loadDataComboModule(newProyect.getIdProyect());
                    idButtonAdd.setDisable(true);
                }

            }
        };
        return listener;
    }

    private ChangeListener<ModuleModel> getChangeListenerModuleModel() {
        ChangeListener<ModuleModel> listener = new ChangeListener<ModuleModel>() {
            @Override
            public void changed(ObservableValue<? extends ModuleModel> ov, ModuleModel oldModule, ModuleModel newModule) {

                if (newModule != null) {
                    idButtonAdd.setDisable(false);
                }
            }
        };
        return listener;
    }

    private void loadDataComboProyect() {
        try {
            List<ProyectModel> listProyectModel = getService().getComboProyect();

            if (listProyectModel != null) {
                ObservableList<ProyectModel> obsList = FXCollections.observableArrayList();
                for (ProyectModel proyect : listProyectModel) {
                    obsList.add(proyect);
                }
                idComboProyect.setItems(obsList);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrudQueryController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadDataComboModule(int idProyect) {
        try {
            List<ModuleModel> listModuleModel = getService().getComboModule(idProyect);
            if (listModuleModel != null) {
                ObservableList<ModuleModel> obsList = FXCollections.observableArrayList();
                for (ModuleModel module : listModuleModel) {
                    obsList.add(module);
                }
                idComboModule.setDisable(false);
                idComboModule.setItems(obsList);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CrudQueryController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
