/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import model.LoginModel;

/**
 *
 * @author capacitacion
 */
/*COMENTARIO DE PRUEBA*/
public class SampleController implements Initializable {

    public LoginModel loginModel = new LoginModel();
    @FXML
    private Label label;
    @FXML
    private TextField txtUsuario;
    @FXML
    private PasswordField txtContrasenia;

    @FXML
    private void handleButtonAction(ActionEvent event) {
        System.out.println("You clicked me!");
        label.setText("Hello World!");
    }

    @FXML
    private void handleButtonActionLogin(ActionEvent event) throws SQLException {

        if (loginModel.isLogin(txtUsuario.getText(), txtContrasenia.getText())) {
            label.setText("Bienvenido " + txtUsuario.getText());
        } else {
            label.setText("Usuario o contraseña incorrecta");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        if (loginModel.isDbConnected()) {
            label.setText("Base de datos, conectada");
        } else {
            label.setText("Base de datos desconectada");
        }
    }
}
