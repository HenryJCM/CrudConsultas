/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.tab.UpdateQueryTabController;
import controller.tab.AddQueryTabController;
import controller.tab.DeleteQueryTabController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import service.CrudQueryService;

/**
 * El controlador se encarga de manejar la vista "AddQueryTab"
 * Es incicializada por el controlador "CrudQueryController"
 * FXML Controller class
 * @author capacitacion
 * @version 1.0 25/08/2016
 * @see controller.tab.AddQueryTabController.java
 * @see controller.tab.DeleteQueryTabController.java
 * @see controller.tab.UpdateQueryTabController.java
 * @see view.CrudQueryController.fxml
 * @see service.CrudQueryService
 * 
 */
public class CrudQueryController implements Initializable {
    
    private CrudQueryService crudQueryService = new CrudQueryService();
    @FXML
    private AddQueryTabController addQueryTabController ;
    @FXML
    private UpdateQueryTabController updateQueryTabController;
    @FXML
    private DeleteQueryTabController deleteQueryTabController;

   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        addQueryTabController.injectMainController(this);
        addQueryTabController.init();

        updateQueryTabController.injectMainController(this);
        updateQueryTabController.init();

        deleteQueryTabController.injectMainController(this);
        deleteQueryTabController.init();
    }

    public CrudQueryService getService() {
        return crudQueryService;
    }
    
    public void refreshAddQueryTab(){
        addQueryTabController.refreshAddQueryTab();
    }
    
    public void refreshUpdateQueryTab(){
        updateQueryTabController.refreshUpdateQueryTab();
    }
    
    public void refreshDeleteQueryTab(){
        deleteQueryTabController.refreshDeleteQueryTab();
    }
}
