/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import repository.SqliteConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author capacitacion
 */
public class LoginModel {

    Connection conection;

    public LoginModel() {
        conection = SqliteConnection.Connector();
        if (conection == null) {
            System.exit(1);
        }
    }

    public boolean isDbConnected() {
        try {
            return !conection.isClosed();
        } catch (Exception e) {
            return false;
        }
    }

    public String getQuery(int idConsulta) throws SQLException {
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        String query = "Select consulta from Consulta where idConsulta = ?";

        try {
            preparedStatement = conection.prepareStatement(query);
            preparedStatement.setString(1, String.valueOf(idConsulta));

            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getString(1);
            }
        } catch (Exception e) {
        } finally {
            preparedStatement.close();
            resultSet.close();
        }

        return null;
    }

    public boolean isLogin(String user, String pass) throws SQLException {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            //String query = "Select * from Usuario where usuario = ? and contrasenia = ?";
            preparedStatement = conection.prepareStatement(getQuery(1));
            preparedStatement.setString(1, user);
            preparedStatement.setString(2, pass);

            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            System.out.println(e);
            return false;
        } finally {
            preparedStatement.close();
            resultSet.close();
        }
    }
}
