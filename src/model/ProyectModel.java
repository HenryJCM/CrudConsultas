/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public class ProyectModel {

    private int idProyect;
    private String nameProyect;

    public ProyectModel(int idProyect, String nameProyect) {
        this.idProyect = idProyect;
        this.nameProyect = nameProyect;
    }

    public int getIdProyect() {
        return idProyect;
    }

    public void setIdProyect(int idProyect) {
        this.idProyect = idProyect;
    }

    public String getNameProyect() {
        return nameProyect;
    }

    public void setNameProyect(String nameProyect) {
        this.nameProyect = nameProyect;
    }

    @Override
    public String toString() {
        return getNameProyect();
    }
}
