
package model;

/**
 *
 * @author Capacitación
 */
public class UserModel {

    private int idUser;
    private String NameUser;
    private String Password;
    private int typeUser;

    public UserModel(int idUser, String NameUser, String Password, int typeUser) {
        this.idUser = idUser;
        this.NameUser = NameUser;
        this.Password = Password;
        this.typeUser = typeUser;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNameUser() {
        return NameUser;
    }

    public void setNameUser(String NameUser) {
        this.NameUser = NameUser;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public int getTypeUser() {
        return typeUser;
    }

    public void setTypeUser(int typeUser) {
        this.typeUser = typeUser;
    }

}
