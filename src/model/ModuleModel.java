/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author capacitacion
 */
public class ModuleModel {

    private int idModule;
    private int idProyect;
    private String nameModule;

    public ModuleModel(int idModule, int idProyect, String nameModule) {
        this.idModule = idModule;
        this.idProyect = idProyect;
        this.nameModule = nameModule;
    }

    public int getIdModule() {
        return idModule;
    }

    public void setIdModule(int idModule) {
        this.idModule = idModule;
    }

    public int getIdProyect() {
        return idProyect;
    }

    public void setIdProyect(int idProyect) {
        this.idProyect = idProyect;
    }

    public String getNameModule() {
        return nameModule;
    }

    public void setNameModule(String nameModule) {
        this.nameModule = nameModule;
    }

    @Override
    public String toString() {
        return getNameModule();
    }
}
