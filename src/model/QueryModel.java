/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author capacitacion
 */
public class QueryModel {

    private int idQuery;
    private int idProyect;
    private int idModule;
    private String query;
    private int numberQuery;
    private String description;

    public QueryModel(int idQuery, int idProyect, int idModule, String query, int numberQuery, String description) {
        this.idQuery = idQuery;
        this.idProyect = idProyect;
        this.idModule = idModule;
        this.query = query;
        this.numberQuery = numberQuery;
        this.description = description;
    }

    public int getIdQuery() {
        return idQuery;
    }

    public void setIdQuery(int idQuery) {
        this.idQuery = idQuery;
    }

    public int getIdProyect() {
        return idProyect;
    }

    public void setIdProyect(int idProyect) {
        this.idProyect = idProyect;
    }

    public int getIdModule() {
        return idModule;
    }

    public void setIdModule(int idModule) {
        this.idModule = idModule;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getNumberQuery() {
        return numberQuery;
    }

    public void setNumberQuery(int numberQuery) {
        this.numberQuery = numberQuery;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return getDescription();
    }
}
