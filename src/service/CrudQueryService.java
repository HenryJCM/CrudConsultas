/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.sql.SQLException;
import java.util.List;
import model.ModuleModel;
import model.ProyectModel;
import model.QueryModel;
import repository.CrudQuery;

/**
 *
 * @author capacitacion
 */
public class CrudQueryService {

    CrudQuery crudQuery = new CrudQuery();

    public List<ProyectModel> getComboProyect() throws SQLException {
        return crudQuery.getComboProyect();
    }

    public List<ModuleModel> getComboModule(int idProyect) throws SQLException {
        return crudQuery.getComboModule(idProyect);
    }

    public boolean saveNewQuery(int idProyect, int idModule, String query, String description) throws SQLException {
        return crudQuery.saveNewQuery(idProyect, idModule, query, description);
    }

    public List<QueryModel> getComboQuery(int idProyect, int idModule) throws SQLException {
        return crudQuery.getComboQuery(idProyect, idModule);
    }

    public boolean deleteQuery(int idProyect, int idModule, int idNumberQuery) throws SQLException {
        return crudQuery.deleteQuery(idProyect, idModule, idNumberQuery);
    }

    public boolean updateQuery(int idQuery, String query, String description) throws SQLException {
        return crudQuery.updateQuery(idQuery, query, description);
    }
}